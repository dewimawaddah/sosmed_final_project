<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFollowersLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('this_user');
            $table->foreign('this_user')->references('id')->on('users');
            $table->unsignedBigInteger('following');
            $table->foreign('following')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followers_logs');
    }
}
