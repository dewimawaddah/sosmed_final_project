@extends('layouts.master')

@section('konten')

<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle"
                        src="{{ asset('themes') }}/dist/img/user2-160x160.jpg" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ $users->name }}</h3>

                <p class="text-muted text-center">{{ date('d F Y', strtotime($users->created_at)) }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Followers</b> <a class="float-right">1,322</a>
                    </li>
                    <li class="list-group-item">
                        <b>Following</b> <a class="float-right">543</a>
                    </li>
                    <li class="list-group-item">
                        <b>Friends</b> <a class="float-right">13,287</a>
                    </li>
                </ul>
                @if(!$is_same_user)
                <a href="#" class="btn btn-primary btn-block follow-btn"><b>Follow</b></a>
                @endif()
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="card">
            <div class="card-header p-2">
                <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Status</a></li>
                    @if($is_same_user)
                    <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                    @endif()
                </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <!-- Post -->
                        @forelse ($posts as $post)
                        <div class="post">
                            <div class="user-block">
                                <img class="img-circle img-bordered-sm"
                                    src="{{ asset('themes') }}/dist/img/user2-160x160.jpg" alt="user image">
                                <span class="username">
                                    <a href="#">{{ $post->name }}</a>
                                    <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                </span>
                                <span class="description">{{ date('d F Y', strtotime($post->created_at)) }}</span>
                            </div>
                            <!-- /.user-block -->
                            <p>
                                {{ $post->posting }}
                            </p>

                            <p>
                                <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                                <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                                <span class="float-right">
                                    <a href="#" class="link-black text-sm">
                                        <i class="far fa-comments mr-1"></i> Comments
                                    </a>
                                </span>
                            </p>

                            <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
                        </div>
                        <!-- /.post -->
                        @empty

                        @endforelse
                    </div>
                    @if ($is_same_user)
                    <div class="tab-pane" id="settings">
                        <form class="form-horizontal" method="post"
                            action="{{ route('profil.update', ['id_update'=>$users->id]) }}">
                            @method('PUT')
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" placeholder="Name"
                                        value="{{ $users->name }}" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-sm-2 col-form-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="username" placeholder="Username"
                                        value="{{ $users->username }}" name="username">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="photo" class="col-sm-2 col-form-label">Photo</label>
                                <div class="col-sm-10">
                                    <input type="file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection


@section('js')
<script>
    function cekfollow(){
        const user_id =  {{$users->id}}
                const params = {
                    user_id: user_id,
                }
                $.get('/status_follow', params).done(result=>{
                    $('.follow-btn').text(result);
                });
    }
    $(document).ready(() => {
        cekfollow();
            $(document).on('click', '.follow-btn', function(e) {
                const user_id =  {{$users->id}}
                const params = {
                    user_id: user_id,
                }
                $.get('/check_follow', params).done(result=>{
                    if(result.data === "Unfollowed") {
                        swal("Sukses", "Berhasil Unfollow User", "success");
                        $(this).text("Follow")
                    } else {
                        swal("Sukses", "Berhasil Follow User", "success");
                        $(this).text("Unfollow")
                    }
                })
            })
        })


</script>
@endsection
