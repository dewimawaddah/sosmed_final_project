<?php

use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('login');
});

Auth::routes(['logout' => false, 'password.reset' => false]);
Route::any('/logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('post', 'PostController');
    Route::get('/profil/{username?}', 'ProfilController@index')->name('profil');
    Route::get('/profil/edit/{id}', 'ProfilController@edit')->name('profil.edit');
    Route::put('/profil/update/{id_update}', 'ProfilController@update')->name('profil.update');
    Route::post('checklike', 'HomeController@checklike')->name('home.checklike');
    Route::post('do_comment', 'HomeController@do_comment')->name('home.do_comment');
    Route::get('check_follow', 'ProfilController@check_follow')->name('profil.check_follow');
    Route::get('status_follow', 'ProfilController@status_follow')->name('profil.status_follow');
});
