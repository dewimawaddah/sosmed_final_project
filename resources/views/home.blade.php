@extends('layouts.master')

@section('konten')
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">Upload Status</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <textarea id="summernote">
              </textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            @csrf
                            <button type="button" class="submit_status btn btn-block btn-primary">Send Status</button>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>

    <div>
        <div class="row">
            <div class="col status">
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).on('click', '.like-btn', function(e) {
        const post_id = $(this).data('postid');
        const _token = $("input[name=_token]").val();
        // console.log(post_id)
        const params = {
            _token: _token,
            post_id: post_id
        }

        $.post(`${BASE_URL}/checklike`, params).done(result => {refreshStatus()})
    });
  function refreshStatus() {
    $('.status').empty()
    $.get(`${BASE_URL}/post`).done(result => {
      const arr = result.data;
      let template = ``;
      arr.forEach(v => {
        template+=`
        <div class="card">
          <div class="card-body">
            <div class="post">
              <div class="user-block">
                <img class="img-circle img-bordered-sm" src="{{ asset('themes') }}/dist/img/user2-160x160.jpg" alt="user image">
                <span class="username">
                  <a href="#">${v.name}</a>
                </span>
                <span class="description">${v.created_at}</span>
              </div>
              <!-- /.user-block -->
              <span>${v.posting}</span>

              <p>
                <a href="#" data-postid=${v.id} class="link-black like-btn text-sm"><i class="far fa-thumbs-up mr-1"></i> Like (${v.total_user_like})</a>
                <span class="float-right">
                  <a href="#" class="link-black text-sm">
                    <i class="far fa-comments mr-1"></i> Comments (${v.total_user_comment})
                  </a>
                </span>
              </p>
              <input class="form-control form-control-sm comment-box" data-postid=${v.id} type="text" placeholder="Type a comment">
            </div>
          </div>
        </div>`
      });
      $('.status').append(template)
    })
  }
  $(function () {
    // Summernote
    $('#summernote').summernote({
      toolbar: [
        ['style', ['style']],
        ['insert', ['picture']]
      ]
    })
    refreshStatus();
  })

  $(document).on('click', '.submit_status', function (e) {
    e.preventDefault();
    const _token = $("input[name=_token]").val();
    const value = $("#summernote").summernote('code');
    const req = {
      "_token": _token,
      "posting": value,
    }
    $.post(`${BASE_URL}/post`, req).done(result => {
      $("#summernote").summernote('reset');
      refreshStatus();
    });
  })
</script>
@endsection
