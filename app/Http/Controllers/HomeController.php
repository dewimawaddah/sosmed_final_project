<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use App\Post;
use App\UserCommentPost;
use App\UserLikePost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $post = Post::all();
        $judul = "Beranda";
        return view('home', compact('judul', 'post'));
    }

    public function checklike(Request $request)
    {
        $user_id = Auth::user()->id;
        $x = UserLikePost::where('post_id', $request->post_id)->where('user_id', "$user_id");
        $count_x = $x->count();
        $data_x = $count_x > 0 ? $x->first() : [];

        if ($count_x > 0) {
            // jika sudah di like
            UserLikePost::where([
                'post_id' => $request->post_id,
                'user_id' => Auth::user()->id
            ])->delete();
            $status = "deleted";
        } else {
            // jika belom di like,
            UserLikePost::create([
                'post_id' => $request->post_id,
                'user_id' => Auth::user()->id
            ]);
            $status = "updated";
        }
        return ResponseFormatter::success($status);
    }

    public function do_comment(Request $request)
    {
        $user_id = Auth::user()->id;
        $ucp = UserCommentPost::create([
            'post_id' => $request->post_id,
            'comment' => $request->comment,
            'commented_by' => $user_id
        ]);

        return ResponseFormatter::success($ucp);
    }
}
