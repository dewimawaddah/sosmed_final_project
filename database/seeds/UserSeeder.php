<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        for ($i = 0; $i < 200; $i++) {
            User::create([
                'name' => $faker->name,
                'username' => $faker->userName,
                'password' => bcrypt('123456'),
                'photo' => 'default.png'

            ]);
        }
    }
}
