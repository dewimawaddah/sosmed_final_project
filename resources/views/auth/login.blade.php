@extends('layouts.auth.master')

@section('konten')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('login') }}">{{ $judul ?? env('APP_NAME') }} - Login</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="mb-3">

                    <div class="input-group">
                        <input id="username" placeholder="Masukkan username" type="text"
                            class="form-control @error('username') is-invalid @enderror" name="username"
                            value="{{ old('username') }}" required autofocus>

                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-id-card"></span>
                            </div>
                        </div>
                    </div>

                    @error('username')
                    <div role="alert" class="text-danger">
                        <small>{{ $message }}</small>
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <div class="input-group">
                        <input id="password" placeholder="Masukkan Password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    @error('password')
                    <div role="alert" class="text-danger">
                        <small>{{ $message }}</small>
                    </div>
                    @enderror
                </div>
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <p class="mt-3 mb-0">
                <a href="{{ route('register') }}" class="text-center">Register a new membership</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
@endsection
