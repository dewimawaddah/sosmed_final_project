<?php

namespace App\Http\Controllers;

use App\FollowersLog;
use App\Helpers\ResponseFormatter;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfilController extends Controller
{
    public function index($username = "")
    {
        $param = $username ?? Auth::user()->id;
        $judul = 'Profil';
        $query = "SELECT a.*, b.username, b.name, b.photo FROM posts a LEFT JOIN users b on a.user_id = b.id";
        if ($username != "") {
            $lowercase = strtolower($username);
            $query .= " WHERE b.username = '$username'";
            $users = User::where('username', $lowercase)->first();
        } else {
            $user_id = Auth::user()->id;
            $query .= " WHERE b.id = $user_id";
            $users = User::find($user_id)->first();
        }
        $is_same_user = $users->id == Auth::user()->id ? true : false;
        $posts = DB::select($query);
        return view('layouts.profil', compact('users', 'judul', 'posts', 'is_same_user'));
    }

    public function edit($id)
    {
        $users = User::find($id);
        return view('layouts.profil', compact('users'));
    }

    public function update(Request $request, $id_update)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required',
        ]);

        User::find($id_update)->update([
            'name' => $request->name,
            'username' =>  $request->username,
        ]);
        return redirect(route('profil'))->with('success', 'Berhasil di Update');
    }

    public function check_follow(Request $request)
    {
        $my_id = Auth::user()->id;
        $user_id = $request->user_id;
        $follow_log = DB::select("SELECT * FROM followers_logs WHERE this_user = '$my_id' AND following = '$user_id'");
        if (count($follow_log) > 0) {
            // kalau dia udah follow
            DB::delete("DELETE FROM followers_logs WHERE this_user = '$my_id' AND following = '$user_id'");
            $status = "Unfollowed";
        } else {
            // kalo belom follow
            FollowersLog::create([
                'this_user' => $my_id,
                'following' => $user_id
            ]);
            $status = "Followed";
        }

        return ResponseFormatter::success($status);
    }

    public function status_follow(Request $request)
    {
        $my_id = Auth::user()->id;
        $user_id = $request->user_id;
        $follow_log = DB::select("SELECT * FROM followers_logs WHERE this_user = '$my_id' AND following = '$user_id'");
        if (count($follow_log) > 0) {
            return "Unfollow";
        }
        return "Follow";
    }
}
