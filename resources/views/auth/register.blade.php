@extends('layouts.auth.master')@section('konten')
<div class="register-box">
    <div class="register-logo">
        <a href="{{ route('login') }}">{{ env('APP_NAME') }} - Register</a>
    </div>
    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">Register a new membership</p>
            <form action="{{ route('register') }}" method="post">
                @csrf
                <div class="mb-3">
                    <div class="input-group">
                        <input value="{{ old('name') }}" type="text" class="form-control" name="name"
                            placeholder="Full name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-id-card"></span>
                            </div>
                        </div>
                    </div>
                    @error('name')
                    <div role="alert" class="text-danger">
                        <small>{{ $message }}</small>
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <div class="input-group">
                        <input value="{{ old('username') }}" type="text" class="form-control" name="username"
                            placeholder="Username">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    @error('username')
                    <div role="alert" class="text-danger">
                        <small>{{ $message }}</small>
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <div class="input-group">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    @error('password')
                    <div role="alert" class="text-danger">
                        <small>{{ $message }}</small>
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <div class="input-group">
                        <input type="password" class="form-control" name="password_confirmation"
                            placeholder="Retype password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </form>
            <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</div>
@endsection