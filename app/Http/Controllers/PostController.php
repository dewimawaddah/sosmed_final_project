<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $followed_users = DB::select('SELECT b.* FROM `followers_logs` a left join users b on b.id = a.following WHERE this_user = ?', [Auth::user()->id]);
        $the_id_of_followed_users = [Auth::user()->id];
        foreach ($followed_users as $key => $value) {
            array_push($the_id_of_followed_users, $value->id);
        }
        $params_in = implode(",", $the_id_of_followed_users);
        $data = DB::select("select a.*, b.name, b.photo, b.username, (select count(*) from user_like_posts c where c.post_id = a.id) as total_user_like, (select count(*) from user_comment_posts d where d.post_id = a.id) as total_user_comment from posts a left join users b on a.user_id = b.id where a.user_id in ($params_in) order by a.id desc");
        return ResponseFormatter::success($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'posting' => 'required|string',
        ]);
        if ($validator->fails()) {
            return ResponseFormatter::error($validator->errors()->toJson(), "error");
        }
        $data = [
            'posting' => $request->posting,
            'user_id' => Auth::user()->id,
        ];
        $post = Post::create($data);
        return ResponseFormatter::success($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
